import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier

RSEED = 50
df = pd.read_csv('/home/eduardo/Documents/UnB/IIA/Projeto_3/clean_data.csv')
print(df.head(10))
labels = np.array(df.pop('label'))
print(labels)