from convertToCSV import convert
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score

RSEED = 9

def main():
    nomeArquivo = "selfie_dataset"

    convert(nomeArquivo)
    df = pd.read_csv("../" + nomeArquivo + ".csv")
    df.pop('NOME')

    
    Y_conj = np.array(df.pop('PONTOS_POPULARIDADE'))
    X_conj = df

    colunas = df.head()  

    bins = np.linspace(1.0, 7.0, num=47)
    Y_binned = np.digitize(Y_conj, bins)

    # X = data, Y = target
    X_train, X_test, Y_train, Y_test = train_test_split(X_conj,
                                         Y_conj, 
                                         stratify = Y_binned,
                                         test_size = 0.3, 
                                         random_state = RSEED)


    
    sc = StandardScaler()
    X_train = sc.fit_transform(X_train)
    X_test = sc.transform(X_test)

    rg = RandomForestRegressor(n_estimators=500, random_state=RSEED, max_features="sqrt")
    print("Treinando... com", rg.__class__.__name__)
    rg.fit(X_train, Y_train)
    predicts = rg.predict(X_test)
    errors = abs(predicts - Y_test)
    # print(rg.feature_importances_)
    
    # Accuracy
    mape = 100 * (errors / Y_test)
    accuracy = 100 - np.mean(mape)
    print("Accuracy: {0:.2f}%".format(accuracy))
    # print("Predicoes: ", predicts)
    # print("Errors: ", errors)
    # print("Score: ", rg.score(X_test, Y_test))

    importances = list(zip(rg.feature_importances_, colunas))
    importances.sort(reverse=True)

    for importancia, nome in importances:
        print("Importancia: {0: <10.3} | {1}".format(importancia, nome))
    
if __name__ == "__main__":
    main()