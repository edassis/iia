def convert(nomeArquivo):
    file = None
    lines = None
    table = []

    file = open("../" + nomeArquivo + ".txt", "r")
    lines = file.readlines()
    file.close()

    table.append("NOME,PONTOS_POPULARIDADE,PARTIAL_FACES,IS_FEMALE,BABY,CHILD,TEENAGER,YOUTH,MIDDLE_AGE,SENIOR,WHITE,BLACK,ASIAN,OVAL_FACE,ROUND_FACE,HEART_FACE,SMILING,MOUTH_OPEN,FROWNING,WEARING_GLASSES,WEARING_SUNGLASSES,WEARING_LIPSTICK,TONGUE_OUT,DUCK_FACE,BLACK_HAIR,BLOND_HAIR,BROWN_HAIR,RED_HAIR,CURLY_HAIR,STRAIGHT_HAIR,BRAID_HAIR,SHOWING_CELLPHONE,USING_EARPHONE,USING_MIRROR,BRACES,WEARING_HAT,HARSH_LIGHTING,DIM_LIGHTING")
    for line in lines:
        line = line.split()
        line = ','.join(line)
        table.append(line)

    file = open("../" + nomeArquivo + ".csv", "w")
    for line in table:
        file.write(line + "\n")
    file.close()
