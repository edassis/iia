# search.py
# ---------
# Licensing Information:  You are free to use or extend these projects for
# educational purposes provided that (1) you do not distribute or publish
# solutions, (2) you retain this notice, and (3) you provide clear
# attribution to UC Berkeley, including a link to http://ai.berkeley.edu.
# 
# Attribution Information: The Pacman AI projects were developed at UC Berkeley.
# The core projects and autograders were primarily created by John DeNero
# (denero@cs.berkeley.edu) and Dan Klein (klein@cs.berkeley.edu).
# Student side autograding was added by Brad Miller, Nick Hay, and
# Pieter Abbeel (pabbeel@cs.berkeley.edu).


"""
In search.py, you will implement generic search algorithms which are called by
Pacman agents (in searchAgents.py).
"""

import util
import searchAgents

class SearchProblem:
    """
    This class outlines the structure of a search problem, but doesn't implement
    any of the methods (in object-oriented terminology: an abstract class).

    You do not need to change anything in this class, ever.
    """

    def getStartState(self):
        """
        Returns the start state for the search problem.
        """
        util.raiseNotDefined()

    def isGoalState(self, state):
        """
          state: Search state

        Returns True if and only if the state is a valid goal state.
        """
        util.raiseNotDefined()

    def getSuccessors(self, state):
        """
          state: Search state

        For a given state, this should return a list of triples, (successor,
        action, stepCost), where 'successor' is a successor to the current
        state, 'action' is the action required to get there, and 'stepCost' is
        the incremental cost of expanding to that successor.
        """
        util.raiseNotDefined()

    def getCostOfActions(self, actions):
        """
         actions: A list of actions to take

        This method returns the total cost of a particular sequence of actions.
        The sequence must be composed of legal moves.
        """
        util.raiseNotDefined()


def tinyMazeSearch(problem):
    """
    Returns a sequence of moves that solves tinyMaze.  For any other maze, the
    sequence of moves will be incorrect, so only use this for tinyMaze.
    """
    from game import Directions
    s = Directions.SOUTH
    w = Directions.WEST
    return  [s, s, w, s, w, w, s, w]

def depthFirstSearch(problem):
    """
    Search the deepest nodes in the search tree first.
    """
    estadoAtual = problem.getStartState()
    res = list()
    estadosVisitados = set()
    
    def DFS(meuEstado):
        
        if meuEstado in estadosVisitados:
            return False
        
        if problem.isGoalState(meuEstado):
            return True

        meusSucessores = problem.getSuccessors(meuEstado)
        estadosVisitados.add(meuEstado)
        
        for successor, action, _ in meusSucessores:
            if DFS(successor):
                res.append(action) 
                return True
        
        return False

    if DFS(estadoAtual):
        res.reverse()
        return res
    else:
        return list()
        

    # util.raiseNotDefined()

def breadthFirstSearch(problem):
    """Search the shallowest nodes in the search tree first."""
    from util import Queue
    
    # fila de estados a serem visitados
    fila = Queue()
    # estado inicial
    estadoAtual = problem.getStartState()
    # lista de direções
    res = list()
    # estados visitados
    estadosVisitados = dict()
    
    fila.push(estadoAtual)
    estadosVisitados[estadoAtual] = (None, None)
    ultimo = None

    while not fila.isEmpty():
        meuEstado = fila.pop()

        if problem.isGoalState(meuEstado):
            ultimo = meuEstado
            break

        meusSucessores = problem.getSuccessors(meuEstado)
        
        for successor, action, _ in meusSucessores:
            if successor not in estadosVisitados.keys():
                estadosVisitados[successor] = (meuEstado, action)
                fila.push(successor)
    
    if ultimo == None:
        return list()

    while ultimo != None:
        successor, action = estadosVisitados[ultimo]
        res.append(action)
        ultimo = successor

    res.pop()
    res.reverse()
    return res
        
    # util.raiseNotDefined()

def uniformCostSearch(problem):
    """Search the node of least total cost first."""
    "*** YOUR CODE HERE ***"
    util.raiseNotDefined()

def heuristicaMediaGeo(position, problem):
    "Faz a média geométrica das heurísticas euclidiana e manhattan"
    
    from searchAgents import euclideanHeuristic, manhattanHeuristic

    euclidiana = euclideanHeuristic(position, problem)
    manhattan = manhattanHeuristic(position, problem)
    return ((manhattan ** 2) * euclidiana) ** (1/3)


def heuristicaMediaArit(position, problem):
    "Faz a média aritmética das heurísticas euclidiana e manhattan"

    from searchAgents import euclideanHeuristic, manhattanHeuristic

    euclidiana = euclideanHeuristic(position, problem)
    manhattan = manhattanHeuristic(position, problem)

    return ((manhattan * 98) + euclidiana * 2) / 100


def heuristicaMediaEuc(position, problem):
    "Faz a média euclidiana das heurísticas euclidiana e manhattan"

    from searchAgents import euclideanHeuristic, manhattanHeuristic

    euclidiana = euclideanHeuristic(position, problem)
    manhattan = manhattanHeuristic(position, problem)

    return ((manhattan ** 2) + euclidiana ** 2) ** .5


def heuristicaAteParede(position, problem):
    """
    Calcula as distãncias vertical e horizontal das paredes
    e faz a razão dos menores valores com o produto das 
    heurísticas euclidiana e manhattan.
    """
    
    from searchAgents import euclideanHeuristic, manhattanHeuristic
    
    res = 0
    euclidiana = euclideanHeuristic(position, problem)
    manhattan = manhattanHeuristic(position, problem)

    x, y = position
    var1 = 0 
    var2 = 0 
    var3 = 0 
    var4 = 0

    while not problem.isWall(x, y):
        var1 += 1
        x += 1


    x, y = position
    while not problem.isWall(x, y):
        var2 += 1
        x -= 1

    x, y = position
    while not problem.isWall(x, y):
        var3 += 1
        y += 1

    x, y = position
    while not problem.isWall(x, y):
        var4 += 1
        y -= 1

    res = (min(var1, var2) + 1) + (min(var3, var4) + 1) 
    return (manhat * euclid) / (res + 0.1)



def nullHeuristic(state, problem=None):
    """
    A heuristic function estimates the cost from the current state to the nearest
    goal in the provided SearchProblem.  This heuristic is trivial.
    """
    return 0

def aStarSearch(problem, heuristic=nullHeuristic):
    """ Search the node that has the lowest combined cost and heuristic first.

        O algoritmo da estrela faz uso de uma fila de prioridade para escolher
        de menor custo aplicando o Dijsktra.
    """

    from util import PriorityQueue
    from game import Directions


    """ 
    A fila de prioridade terá no começo o próximo estado com o menor custo
    possível.
    """
    fila = PriorityQueue()
    estadoAtual = problem.getStartState()   # estado atual do pacman
    res = list()                            # lista de direções a serem tomadas
    estadoVisitados = dict()                # estados (nós) já tomados
    
    # estado inicial
    fila.push((None, estadoAtual, 0, Directions.STOP), 0 + heuristic(estadoAtual, problem))
    ultimo = None

    # enquanto a fila não estiver vazia (possíveis estados a serem tomados)
    while not fila.isEmpty():
        meuPai, meuEstado, meuCusto, minhaDirecao = fila.pop()

        # caso o estado atual já tenha sido visitado
        if meuEstado in estadoVisitados.keys():
            continue

        # vizinhos do estado atual
        meusSucessores = problem.getSuccessors(meuEstado)
        # colocando como estado visitado
        estadoVisitados[meuEstado] = (meuPai, minhaDirecao)

        if problem.isGoalState(meuEstado):
            ultimo = meuEstado
            break

        # colocando vizinhos do estado atual na fila
        for successor, action, custo in meusSucessores:
            if successor not in estadoVisitados.keys():
                fila.push((meuEstado, successor, meuCusto + custo, action), (custo + meuCusto) + heuristic(successor, problem))
    
    # Não encontrou o caminho para o objetivo.
    if ultimo == None:
        return list()

    # Encontrou o caminho para o objetivo.
    while ultimo != None:
        successor, action = estadoVisitados[ultimo]
        res.append(action)
        ultimo = successor
    
    res.pop()
    res.reverse()
    return res



    # util.raiseNotDefined()



# Abbreviations
bfs = breadthFirstSearch
dfs = depthFirstSearch
astar = aStarSearch
ucs = uniformCostSearch