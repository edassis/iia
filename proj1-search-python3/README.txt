Ambiente:
Python 3.7.3 (default, Apr  3 2019, 19:16:38) 
[GCC 8.0.1 20180414 (experimental) [trunk revision 259383]] on linux
Type "help", "copyright", "credits" or "license" for more information.


Integrantes:
- Eduardo Ferreira de Assis - 17/0102289
- Oscar Etcheaverry Barbosa Madureira da Silva - 17/0112209


Mudanças:
- search.py:
    -- Funções implementadas:
        -> DFS();
        -> BFS();
        -> aStarSearch();
        -> heuristicaMediaGeo();
        -> heuristicaMediaArit();
        -> heuristicaMediaEuc();
        -> heuristicaAteParede.
      
- searchAgents.py:
    -- Funções implementadas:
        -> método isWall() em PositionSearchProblem.