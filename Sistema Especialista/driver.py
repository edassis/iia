# driver.py

from __future__ import with_statement
import sys
from pyke import knowledge_engine
from pyke import krb_traceback
import os

# Limpar o terminal.
if os.name == 'nt':
    def clear():
        os.system('cls')
else:
    def clear():
        os.system('clear')


def realizarDiagnostico(engine, sintomas):
    """ Realiza o diagnostico atraves do sistema especialista (Pyke). """

    res = "Saudavel"
    engine.reset()
    try:
        engine.activate('regras')
        a2, b2, c2, d2, e2, f2, g2, h2, i2, j2, k2, l2 = sintomas
        with engine.prove_goal('regras.solve($res, $a1, $b1, $c1, $d1, $e1, $f1, $g1, $h1, $i1, $j1, $k1, $l1)',
                               a1=a2, b1=b2, c1=c2, d1=d2, e1=e2, f1=f2, g1=g2, h1=h2, i1=i2, j1=j2, k1=k2, l1=l2) \
                as gen:
            flag = False

            for (vars, no_plan) in gen:
                flag = True
                res = vars['res']
                break

            if not flag:
                res = "Outra doenca"
    except:
        krb_traceback.print_exc()
        sys.exit(1)
    return res


def inputCorreto(funcVerifica, string):
    """ Checa validade do input. """

    res = None
    while True:

        clear()
        try:
            res = input(string)
        except:
            res = ""
        
        if funcVerifica(res):
            break
        else:
            clear()
            print("Entrada Invalida!")
            print("Aperte <ENTER> e digite a opcao correta!")
            while raw_input() != "":
                pass
    return res


def febre(lista):
    """ Analise da febre. """

    tem = 0
    intensidade = 0
    duracao = 0

    tem = inputCorreto(lambda a: (type(a).__name__ == "int" and (a == 0 or a == 1)),
                       "Voce apresenta febre (1 = sim, 0 = nao)? ")
    lista[0] = tem

    if tem == 1:
        intensidade = inputCorreto(lambda a: (type(a).__name__ == "int" and (a == 1 or a == 2)),
                                   "Qual a intensidade (1 = subfebril, 2 = febre alta)? ")
        duracao = inputCorreto(lambda a: (type(a).__name__ == "int" and (a >= 1 and a <= 7)),
                               "Qual a duracao (1 a 7 dias)? ")

    return (tem, (intensidade, duracao))


def manchas(lista):
    """ Analise de manchas. """

    tem = 0
    intervalo1 = 0
    intervalo2 = 0

    tem = inputCorreto(lambda a: (type(a).__name__ == "int" and (a == 0 or a == 1)),
                       "Voce apresenta manchas na pele (1 = sim, 0 = nao)? ")
    lista[1] = tem

    if tem == 1:
        intervalo = inputCorreto(lambda a: (type(a).__name__ == "int" and (a > 0 and a < 1000)),
                                 "Quando as manchas apareceram (1 = primeiro dia, 2 = primeiro dia, ...)? ")
        if intervalo <= 4:
            intervalo1 = intervalo
        else:
            intervalo2 = 1

    return (tem, (intervalo1, intervalo2))


def dorMusculos(lista):
    """ Analise de dores nos musculos. """

    tem = 0
    val = 0

    tem = inputCorreto(lambda a: (type(a).__name__ == "int" and (a == 0 or a == 1)),
                       "Voce apresenta dor nos musculos (1 = sim, 0 = nao)? ")
    lista[2] = tem

    if tem == 1:
        val = inputCorreto(lambda a: (type(a).__name__ == "int" and (a > 0 and a < 4)),
                           "Qual a frequencia das dores (1 = 1/3, 2 = 2/3, 3 = 3/3)? ")

    return (tem, val)


def dorArticulacao(lista):
    """ Analise de dores nas articulacoes. """

    tem = 0
    val = 0

    tem = inputCorreto(lambda a: (type(a).__name__ == "int" and (a == 0 or a == 1)),
                       "Voce apresenta dor nas articulacoes (1 = sim, 0 = nao)? ")
    lista[3] = tem

    if tem == 1:
        val = inputCorreto(lambda a: (type(a).__name__ == "int" and (a > 0 and a < 4)),
                           "Qual a frequencia das dores (1 = 1/3, 2 = 2/3, 3 = 3/3)? ")

    return (tem, val)


def intArticulacao(lista):
    tem = 0
    val = 0

    tem = lista[3]
    lista[4] = tem

    if tem == 1:
        val = inputCorreto(lambda a: (type(a).__name__ == "int" and (a > 0 and a < 4)),
                           "Qual a intensidade dores (1 = leve, 2 = moderada, 3 = intensa)? ")

    return (tem, val)


def endArticulacao(lista):
    tem = 0
    val = 0

    tem = inputCorreto(lambda a: (type(a).__name__ == "int" and (a == 0 or a == 1)),
                       "Voce apresenta endema nas articulacoes (1 = sim, 0 = nao)? ")
    lista[5] = tem

    if tem == 1:
        val = inputCorreto(lambda a: (type(a).__name__ == "int" and (a > 0 and a < 4)),
                           "Qual a intensidade (1 = leve, 2 = moderada, 3 = intensa)? ")

    return (tem, val)


def conjuntivite(lista):
    """ Analise de conjuntivite. """

    tem = 0

    tem = inputCorreto(lambda a: (type(a).__name__ == "int" and (a == 0 or a == 1)),
                       "Voce esta com conjuntivite (1 = sim, 0 = nao)? ")
    lista[6] = tem

    return (tem)


def dorCabeca(lista):
    """ Analise de dor de cabeca. """

    tem = 0
    val = 0

    tem = inputCorreto(lambda a: (type(a).__name__ == "int" and (a == 0 or a == 1)),
                       "Voce apresenta dor de cabeca (1 = sim, 0 = nao)? ")
    lista[7] = tem

    if tem == 1:
        val = inputCorreto(lambda a: (type(a).__name__ == "int" and (a > 0 and a < 4)),
                           "Qual a intensidade (1 = leve, 2 = moderada, 3 = intensa)? ")

    return (tem, val)


def coceira(lista):
    """ Analise de coceira. """

    tem = 0
    val = 0

    tem = inputCorreto(lambda a: (type(a).__name__ == "int" and (a == 0 or a == 1)),
                       "Voce apresenta coceira no corpo (1 = sim, 0 = nao)? ")
    lista[8] = tem

    if tem == 1:
        val = inputCorreto(lambda a: (type(a).__name__ == "int" and (a > 0 and a < 4)),
                           "Qual a intensidade (1 = leve, 2 = moderada, 3 = intensa)? ")

    return (tem, val)


def ganglionar(lista):
    """ Analise de  """

    tem = 0
    val = 0

    tem = inputCorreto(lambda a: (type(a).__name__ == "int" and (a == 0 or a == 1)),
                       "Voce apresenta hipertrofia ganglionar (1 = sim, 0 = nao)? ")
    lista[9] = tem

    if tem == 1:
        val = inputCorreto(lambda a: (type(a).__name__ == "int" and (a > 0 and a < 4)),
                           "Qual a intensidade (1 = leve, 2 = moderada, 3 = intensa)? ")

    return (tem, val)


def hemorragica(lista):
    """ Analise  """

    tem = 0
    val = 0

    tem = inputCorreto(lambda a: (type(a).__name__ == "int" and (a == 0 or a == 1)),
                       "Voce apresenta discrasia hemorragica (1 = sim, 0 = nao)? ")
    lista[10] = tem

    if tem == 1:
        val = inputCorreto(lambda a: (type(a).__name__ == "int" and (a > 0 and a < 4)),
                           "Qual a intensidade (1 = leve, 2 = moderada, 3 = intensa)? ")

    return (tem, val)


def neurologico(lista):
    """ Analise de dores nas articulacoes. """

    tem = 0

    tem = inputCorreto(lambda a: (type(a).__name__ == "int" and (a == 0 or a == 1)),
                       "Voce apresenta acometimento neurologico (1 = sim, 0 = nao)? ")
    lista[11] = tem

    return (tem)


def sistemaEspecialista(engine):
    """Funcao mestra para o diagnostico do usuario.  

        Faz uma serie de chamadas para obter informacoes a respeito dos
        sintomas do usuario. Depois realiza a avaliacao com o Sistema
        Especialista.
    """

    lista = [0 for i in range(12)]
    sintomas = [febre(lista), manchas(lista), dorMusculos(lista), dorArticulacao(lista),
                intArticulacao(lista), endArticulacao(lista), conjuntivite(lista),
                dorCabeca(lista), coceira(lista), ganglionar(lista), hemorragica(lista),
                neurologico(lista)]
    res = realizarDiagnostico(engine, sintomas)
    if res != "Saudavel":
        res = "com " + res

    aux = "Voce esta %s\n" % res

    if res == "Saudavel":
        aux += "Parabens!\n\n"

    aux += "Aperte <ENTER> para voltar ao menu principal"

    print(aux)
    while raw_input() != "": pass


def instrucoes():
    """ Exibe as instrucoes para o usuario. """
    clear()
    print("Instrucoes:\n")
    print("1 - Realizar diagnostico:\n")
    print("    Atraves do uso de um sistema especialista, que contem um conjunto de regras\n"
          + "e fatos pre-definidos, realiza o diagnostico do usuario atraves dos sintomas\n"
          + "fornecidos. Caso nao se enquadre em nenhuma das doencas conhecidas\n"
          + "(dengue, zica, chikungunya) ele dirah que eh outra doenca.\n")
    print("2 - Instrucoes:\n")
    print("    Mostra a tela atual.\n")
    print("3 - Sair:\n")
    print("    Termina a execucao do programa.\n")
    print("Aperte <ENTER> para voltar ao menu principal.")

    while raw_input() != "": pass

def main():
    """ Funcao principal do programa. """

    engine = knowledge_engine.engine(__file__)
    sair = False
    while not sair:
        s1 = "Opcoes:\n"
        s2 = "\t1 - Realizar diagnostico.\n"
        s3 = "\t2 - Instrucoes.\n"
        s4 = "\t3 - Sair.\n"
        s5 = "> Digite a opcao desejada: "
        opcao = inputCorreto(lambda a: (type(a).__name__ == "int" and (
            a >= 1 and a <= 3)), s1 + s2 + s3 + s4 + s5)

        if opcao == 1:
            sistemaEspecialista(engine)
        elif opcao == 2:
            instrucoes()
        elif opcao == 3:
            sair = True

    return 0


if __name__ == "__main__":
    main()
