
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score
from sklearn.preprocessing import LabelEncoder

from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Convolution2D 
from keras.layers import ZeroPadding2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
from keras.layers import Dropout
from keras.models import model_from_json
from keras.utils import np_utils
from keras import backend as K
K.common.set_image_dim_ordering('th')
from PIL import Image
import numpy as np
from skimage import transform

RSEED = 9
def convert(nomeArquivo):
    file = None
    lines = None
    table = []

    file = open(nomeArquivo + ".txt", "r")
    lines = file.readlines()
    file.close()

    table.append("NOME,PONTOS_POPULARIDADE,PARTIAL_FACES,IS_FEMALE,BABY,CHILD,TEENAGER,YOUTH,MIDDLE_AGE,SENIOR,WHITE,BLACK,ASIAN,OVAL_FACE,ROUND_FACE,HEART_FACE,SMILING,MOUTH_OPEN,FROWNING,WEARING_GLASSES,WEARING_SUNGLASSES,WEARING_LIPSTICK,TONGUE_OUT,DUCK_FACE,BLACK_HAIR,BLOND_HAIR,BROWN_HAIR,RED_HAIR,CURLY_HAIR,STRAIGHT_HAIR,BRAID_HAIR,SHOWING_CELLPHONE,USING_EARPHONE,USING_MIRROR,BRACES,WEARING_HAT,HARSH_LIGHTING,DIM_LIGHTING")
    for line in lines:
        line = line.split()
        line = ','.join(line)
        table.append(line)

    file = open(nomeArquivo + ".csv", "w")
    for line in table:
        file.write(line + "\n")
    file.close()

def vgg16():
    classifier = Sequential()
    classifier.add(ZeroPadding2D((1,1),input_shape=(64,64,3)))
    classifier.add(Convolution2D(64, (3, 3), activation='relu'))
    classifier.add(ZeroPadding2D((1,1)))
    classifier.add(Convolution2D(64, (3, 3), activation='relu'))
    classifier.add(MaxPooling2D((2,2), strides=(2,2)))

    classifier.add(ZeroPadding2D((1,1)))
    classifier.add(Convolution2D(128, (3, 3), activation='relu'))
    classifier.add(ZeroPadding2D((1,1)))
    classifier.add(Convolution2D(128, (3, 3), activation='relu'))
    classifier.add(MaxPooling2D((2,2), strides=(2,2)))

    classifier.add(ZeroPadding2D((1,1)))
    classifier.add(Convolution2D(256, (3, 3), activation='relu'))
    classifier.add(ZeroPadding2D((1,1)))
    classifier.add(Convolution2D(256, (3, 3), activation='relu'))
    classifier.add(ZeroPadding2D((1,1)))
    classifier.add(Convolution2D(256, (3, 3), activation='relu'))
    classifier.add(MaxPooling2D((2,2), strides=(2,2)))

    classifier.add(ZeroPadding2D((1,1)))
    classifier.add(Convolution2D(512, (3, 3), activation='relu'))
    classifier.add(ZeroPadding2D((1,1)))
    classifier.add(Convolution2D(512, (3, 3), activation='relu'))
    classifier.add(ZeroPadding2D((1,1)))
    classifier.add(Convolution2D(512, (3, 3), activation='relu'))
    classifier.add(MaxPooling2D((2,2), strides=(2,2)))

    classifier.add(ZeroPadding2D((1,1)))
    classifier.add(Convolution2D(512, (3, 3), activation='relu'))
    classifier.add(ZeroPadding2D((1,1)))
    classifier.add(Convolution2D(512, (3, 3), activation='relu'))
    classifier.add(ZeroPadding2D((1,1)))
    classifier.add(Convolution2D(512, (3, 3), activation='relu'))
    classifier.add(MaxPooling2D((2,2), strides=(2,2)))

    classifier.add(Flatten())
    classifier.add(Dense(4096, activation='relu'))
    classifier.add(Dropout(0.5))
    classifier.add(Dense(4096, activation='relu'))
    classifier.add(Dropout(0.5))
    classifier.add(Dense(3, activation='softmax'))
    classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])

    return classifier

def cnn():
    classifier = Sequential()

    classifier.add(Convolution2D(32, 3, 3, input_shape = (64, 64, 3), activation = 'relu'))

    classifier.add(MaxPooling2D(pool_size = (2, 2)))

    classifier.add(Convolution2D(32, 3, 3, activation = 'relu'))
    classifier.add(MaxPooling2D(pool_size = (2, 2)))

    classifier.add(Flatten())

    classifier.add(Dense(output_dim = 128, activation = 'relu'))
    classifier.add(Dense(output_dim = 3, activation = 'softmax'))

    classifier.compile(optimizer = 'adam', loss = 'binary_crossentropy', metrics = ['accuracy'])


    return classifier

def load(filename):
    np_image = Image.open(filename)
    np_image = np.array(np_image).astype('float32')/255
    np_image = transform.resize(np_image, (64, 64, 3))
    np_image = np.expand_dims(np_image, axis=0)
    return np_image

def main(nome):
    nomeArquivo = "selfie_dataset"

    convert(nomeArquivo)
    df = pd.read_csv(nomeArquivo + ".csv")
    df.pop("PARTIAL_FACES")
    df.pop("IS_FEMALE")
    df.pop("BABY")
    df.pop("CHILD")
    df.pop("TEENAGER")
    df.pop("YOUTH")
    df.pop("MIDDLE_AGE")
    df.pop("SENIOR")
    df.pop("WHITE")
    df.pop("BLACK")
    df.pop("ASIAN")
    df.pop("OVAL_FACE")
    df.pop("ROUND_FACE")
    df.pop("HEART_FACE")
    df.pop("SMILING")
    df.pop("MOUTH_OPEN")
    df.pop("FROWNING")
    df.pop("WEARING_GLASSES")
    df.pop("WEARING_SUNGLASSES")
    df.pop("WEARING_LIPSTICK")
    df.pop("TONGUE_OUT")
    df.pop("DUCK_FACE")
    df.pop("BLACK_HAIR")
    df.pop("BLOND_HAIR")
    df.pop("BROWN_HAIR")
    df.pop("RED_HAIR")
    df.pop("CURLY_HAIR")
    df.pop("STRAIGHT_HAIR")
    df.pop("BRAID_HAIR")
    df.pop("SHOWING_CELLPHONE")
    df.pop("USING_EARPHONE")
    df.pop("USING_MIRROR")
    df.pop("BRACES")
    df.pop("WEARING_HAT")
    df.pop("HARSH_LIGHTING")
    df.pop("DIM_LIGHTING")

    colunas = df.head()  
    menorValor = min(df["PONTOS_POPULARIDADE"].tolist())
    maiorValor = max(df["PONTOS_POPULARIDADE"].tolist())
    
    lista = df["PONTOS_POPULARIDADE"].tolist()
    lista.sort()
    bins = [lista[0], lista[int(len(lista)/3)], lista[int((len(lista)*2)/3)], lista[len(lista)-1]]
    # bins = np.linspace(menorValor, maiorValor, num=4)
    
    df["PONTOS_POPULARIDADE"] = np.digitize(df["PONTOS_POPULARIDADE"], bins)

    df["NOME"] = df["NOME"].apply(lambda x: x+".jpg")
    df["PONTOS_POPULARIDADE"] = df["PONTOS_POPULARIDADE"].apply(lambda x: "ruim" if x == 1 else "neutra" if x == 2 else "boa")
    
    Y_conj = df.pop("PONTOS_POPULARIDADE")
    stategy = Y_conj
    X_conj = df

   # encode class values as integers
    encoder = LabelEncoder()
    encoder.fit(Y_conj)
    encoded_Y = encoder.transform(Y_conj)
    # convert integers to dummy variables (i.e. one hot encoded)
    dummy_y = np_utils.to_categorical(encoded_Y)
    dummy_y = dummy_y.tolist()
    dummy_y = list(map(list, zip(*dummy_y)))
    Y_conj = pd.DataFrame({'BOA': dummy_y[0], 'NEUTRA': dummy_y[1], 'RUIM': dummy_y[2]})

    df = pd.concat([X_conj, Y_conj], axis=1, sort=False)
    print(df)
   
    X_train, X_test, _, _ = train_test_split(df,
                                         stategy, 
                                         stratify = stategy,
                                         test_size = 0.3, 
                                         random_state = RSEED)


    classifier = vgg16()

    train_datagen = ImageDataGenerator(rescale = 1./255,
                                    shear_range = 0.2,
                                    zoom_range = 0.2,
                                    horizontal_flip = True)

    test_datagen = ImageDataGenerator(rescale = 1./255)

    training_set = train_datagen.flow_from_dataframe(X_train,
                                                    directory = 'images',
                                                    x_col = "NOME",
                                                    y_col = ["BOA", "NEUTRA", "RUIM"],
                                                    target_size = (64, 64),
                                                    batch_size = 32,
                                                    class_mode = 'raw')

    test_set = test_datagen.flow_from_dataframe(X_test,
                                                directory = 'images',
                                                x_col = "NOME",
                                                y_col = ["BOA", "NEUTRA", "RUIM"],
                                                target_size = (64, 64),
                                                batch_size = 32,
                                                class_mode = 'raw')
    

    classifier.fit_generator(training_set,
                            samples_per_epoch = len(X_train),
                            nb_epoch = 25,
                            validation_data = test_set,
                            nb_val_samples = len(X_test))
    pred = classifier.predict_generator(test_set, steps=1, verbose=1)
    print(pred)
    


    classifier_json = classifier.to_json()
    with open("classifier_VGG.json", "w") as json_file:
        json_file.write(classifier_json)
    # serialize weights to HDF5
    classifier.save_weights("classifier_VGG.h5")
    print("Saved model to disk")

#//
    
    # # later...
    
    # # load json and create model
    # json_file = open('classifier.json', 'r')
    # loaded_classifier_json = json_file.read()
    # json_file.close()
    # loaded_classifier = model_from_json(loaded_classifier_json)
    # # load weights into new model
    # loaded_classifier.load_weights("classifier.h5")
    # print("Loaded model from disk")


    # image = load(X_test["NOME"].tolist()[0])
    # classifier.predict(image)


    # rg = RandomForestRegressor(n_estimators=500, random_state=RSEED, max_features="sqrt")
    # print("Treinando... com", rg.__class__.__name__)
    # rg.fit(X_train, Y_train)
    # predicts = rg.predict(X_test)
    # errors = abs(predicts - Y_test)
    # # print(rg.feature_importances_)
    
    # # Accuracy
    # mape = 100 * (errors / Y_test)
    # accuracy = 100 - np.mean(mape)
    # print("Accuracy: {0:.2f}%".format(accuracy))
    # # print("Predicoes: ", predicts)
    # # print("Errors: ", errors)
    # # print("Score: ", rg.score(X_test, Y_test))

    # importaces = list(zip(rg.feature_importances_, colunas))
    # importaces.sort(reverse=True)

    # for importancia, nome in importaces:
    #     print("Importancia: {0: <10.3} | {1}".format(importancia, nome))
    
if __name__ == "__main__":
    # nomeArquivo = "selfie_dataset"
    # convert(nomeArquivo)
    # df = pd.read_csv("../" + nomeArquivo + ".csv")
    # df.pop('NOME')
    # df.pop('PONTOS_POPULARIDADE')
    # df = df.head()
    # df = df
    # for i in df:
    main("")
        
