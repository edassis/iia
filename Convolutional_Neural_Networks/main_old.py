import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier, RandomForestRegressor
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import classification_report, confusion_matrix, accuracy_score

from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Convolution2D
from keras.layers import MaxPooling2D
from keras.layers import Flatten
from keras.layers import Dense
from keras.optimizers import SGD
from keras.losses import categorical_crossentropy
from keras.models import model_from_json
from PIL import Image
import numpy as np
from skimage import transform

RSEED = 9

def convert(nomeArquivo):
    file = None
    lines = None
    table = []

    file = open(nomeArquivo + ".txt", "r")
    lines = file.readlines()
    file.close()

    table.append("NOME,PONTOS_POPULARIDADE,PARTIAL_FACES,IS_FEMALE,BABY,CHILD,TEENAGER,YOUTH,MIDDLE_AGE,SENIOR,WHITE,BLACK,ASIAN,OVAL_FACE,ROUND_FACE,HEART_FACE,SMILING,MOUTH_OPEN,FROWNING,WEARING_GLASSES,WEARING_SUNGLASSES,WEARING_LIPSTICK,TONGUE_OUT,DUCK_FACE,BLACK_HAIR,BLOND_HAIR,BROWN_HAIR,RED_HAIR,CURLY_HAIR,STRAIGHT_HAIR,BRAID_HAIR,SHOWING_CELLPHONE,USING_EARPHONE,USING_MIRROR,BRACES,WEARING_HAT,HARSH_LIGHTING,DIM_LIGHTING")
    for line in lines:
        line = line.split()
        line = ','.join(line)
        table.append(line)

    file = open(nomeArquivo + ".csv", "w")
    for line in table:
        file.write(line + "\n")
    file.close()

def load(filename):
    np_image = Image.open(filename)
    np_image = np.array(np_image).astype('float32')/255
    np_image = transform.resize(np_image, (64, 64, 3))
    np_image = np.expand_dims(np_image, axis=0)
    return np_image

def main(nome):
    nomeArquivo = "selfie_dataset"

    convert(nomeArquivo)
    df = pd.read_csv(nomeArquivo + ".csv")
    df.pop("PARTIAL_FACES")
    df.pop("IS_FEMALE")
    df.pop("BABY")
    df.pop("CHILD")
    df.pop("TEENAGER")
    df.pop("YOUTH")
    df.pop("MIDDLE_AGE")
    df.pop("SENIOR")
    df.pop("WHITE")
    df.pop("BLACK")
    df.pop("ASIAN")
    df.pop("OVAL_FACE")
    df.pop("ROUND_FACE")
    df.pop("HEART_FACE")
    df.pop("SMILING")
    df.pop("MOUTH_OPEN")
    df.pop("FROWNING")
    df.pop("WEARING_GLASSES")
    df.pop("WEARING_SUNGLASSES")
    df.pop("WEARING_LIPSTICK")
    df.pop("TONGUE_OUT")
    df.pop("DUCK_FACE")
    df.pop("BLACK_HAIR")
    df.pop("BLOND_HAIR")
    df.pop("BROWN_HAIR")
    df.pop("RED_HAIR")
    df.pop("CURLY_HAIR")
    df.pop("STRAIGHT_HAIR")
    df.pop("BRAID_HAIR")
    df.pop("SHOWING_CELLPHONE")
    df.pop("USING_EARPHONE")
    df.pop("USING_MIRROR")
    df.pop("BRACES")
    df.pop("WEARING_HAT")
    df.pop("HARSH_LIGHTING")
    df.pop("DIM_LIGHTING")

    X_conj = df

    colunas = df.head()  
    menorValor = min(df["PONTOS_POPULARIDADE"].tolist())
    maiorValor = max(df["PONTOS_POPULARIDADE"].tolist())
    
    bins = np.linspace(menorValor, maiorValor, num=4)

    df["PONTOS_POPULARIDADE"] = np.digitize(df["PONTOS_POPULARIDADE"], bins)
    
    print(nome, df)

    df["NOME"] = df["NOME"].apply(lambda x: x+".jpg")
    df["PONTOS_POPULARIDADE"] = df["PONTOS_POPULARIDADE"].apply(lambda x: "ruim" if x == 1 else "neutra" if x == 2 else "boa")
    
    print(nome, df)
    print(nome, df["PONTOS_POPULARIDADE"].tolist().count("ruim"), df["PONTOS_POPULARIDADE"].tolist().count("neutra"), df["PONTOS_POPULARIDADE"].tolist().count("boa"))

    

   
    X_train, X_test, Y_train, Y_test = train_test_split(df,
                                         df["PONTOS_POPULARIDADE"], 
                                         stratify = df["PONTOS_POPULARIDADE"],
                                         test_size = 0.3, 
                                         random_state = RSEED)


    print(X_train)
    print(X_test)
    classifier = Sequential()

    # Step 1 - Convolution
    classifier.add(Convolution2D(32, kernel_size=(5, 5), strides=(1, 1),
                                 activation='relu',
                                 input_shape=(64, 64, 3)))

    # Step 2 - Pooling
    classifier.add(MaxPooling2D(pool_size = (2, 2), 
                                strides=(2, 2)))

    # Adding a second convolutional layer
    classifier.add(Convolution2D(64, kernel_size=(5, 5), activation = 'relu'))
    classifier.add(MaxPooling2D(pool_size = (2, 2)))

    # Step 3 - Flattening
    classifier.add(Flatten())

    # Step 4 - Full connection
    classifier.add(Dense(1000, activation = 'relu'))
    classifier.add(Dense(1, activation = 'softmax'))

    # Compiling the CNN
    classifier.compile(loss = categorical_crossentropy,optimizer = SGD(lr=0.01), metrics = ['accuracy'])

    # Part 2 - Fitting the CNN to the images


    train_datagen = ImageDataGenerator(rescale = 1./255,
                                    shear_range = 0.2,
                                    zoom_range = 0.2,
                                    horizontal_flip = True)

    test_datagen = ImageDataGenerator(rescale = 1./255)

    training_set = train_datagen.flow_from_dataframe(X_train,
                                                    directory = 'images',
                                                    x_col = "NOME",
                                                    y_col = "PONTOS_POPULARIDADE",
                                                    target_size = (64, 64),
                                                    batch_size = 32,
                                                    class_mode = 'sparse')

    test_set = test_datagen.flow_from_dataframe(X_test,
                                                directory = 'images',
                                                x_col="NOME",
                                                y_col="PONTOS_POPULARIDADE",
                                                target_size = (64, 64),
                                                batch_size = 32,
                                                class_mode = 'sparse')

    # classifier.fit(training_set, Y_train,
    #                 epochs=10,
    #                 verbose=1,
    #                 validation_data=(test_set, Y_test),
    #                 )
    classifier.fit_generator(training_set,
                            samples_per_epoch = 8000,
                            nb_epoch = 25,
                            validation_data = test_set,
                            nb_val_samples = 2000)
    
    # classifier.fit(x = )

    # classifier_json = classifier.to_json()
    # with open("classifier.json", "w") as json_file:
    #     json_file.write(classifier_json)
    # # serialize weights to HDF5
    # classifier.save_weights("classifier.h5")
    # print("Saved model to disk")
    
    # # later...
    
    # # load json and create model
    # json_file = open('classifier.json', 'r')
    # loaded_classifier_json = json_file.read()
    # json_file.close()
    # loaded_classifier = model_from_json(loaded_classifier_json)
    # # load weights into new model
    # loaded_classifier.load_weights("classifier.h5")
    # print("Loaded model from disk")


    # image = load(X_test["NOME"].tolist()[0])
    # classifier.predict(image)
  
if __name__ == "__main__":
    # nomeArquivo = "selfie_dataset"
    # convert(nomeArquivo)
    # df = pd.read_csv("../" + nomeArquivo + ".csv")
    # df.pop('NOME')
    # df.pop('PONTOS_POPULARIDADE')
    # df = df.head()
    # df = df
    # for i in df:
    main("")
        
